/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include <nfc/nfc.h>
#include <string.h>
#include <stdlib.h>

#include "nfcAccess.h"


static nfc_device *Pnd = NULL;
static nfc_context *Context;

int nfcAccess_Init(void)
{
    nfc_target nt;
    nfc_init(&Context);
    if (Context == NULL) {
      printf("Unable to init libnfc (malloc)\n");
      exit(EXIT_FAILURE);
    }
    const char *acLibnfcVersion = nfc_version();

    printf("Uses libnfc %s\n", acLibnfcVersion);

    Pnd = nfc_open(Context, NULL);

    if (Pnd == NULL) {
      printf("ERROR: %s", "Unable to open NFC device.");
      exit(EXIT_FAILURE);
    }
    if (nfc_initiator_init(Pnd) < 0) {
      nfc_perror(Pnd, "nfc_initiator_init");
      exit(EXIT_FAILURE);
    }

    printf("NFC reader: %s opened\n", nfc_device_get_name(Pnd));
}

void nfcAccess_PullTag(void)
{
    nfc_target nt;
    
    if(Pnd == NULL)
    {
        printf("nfc device not initialized");
        return;
    }
    const nfc_modulation nmMifare = {
      .nmt = NMT_ISO14443A,
      .nbr = NBR_106,
    };
    
    // nfc_set_property_bool(Pnd, NP_AUTO_ISO14443_4, true);
    printf("Polling for target...\n");
    while (nfc_initiator_select_passive_target(Pnd, nmMifare, NULL, 0, &nt) <= 0);
    printf("Target detected!\n");
}

int nfcAccess_CardTransmit(uint8_t * capdu, size_t capdulen, uint8_t * rapdu, size_t * rapdulen)
{
    int transRes;
    size_t  szPos;
  
    if(Pnd == NULL)
    {
        printf("nfc device not initialized");
        goto exitFail;
    }
  
    printf("=> ");
    for (szPos = 0; szPos < capdulen; szPos++) 
    {
      printf("%02x ", capdu[szPos]);
    }
    printf("\n");
    if ((transRes = nfc_initiator_transceive_bytes(Pnd, capdu, capdulen, rapdu, *rapdulen, 500)) < 0) 
    {
      printf("CardTransmit - result fault \n");  
      goto exitFail;
    } 

    *rapdulen = (size_t) transRes;
    printf("<= ");
    for (szPos = 0; szPos < *rapdulen; szPos++) 
    {
      printf("%02x ", rapdu[szPos]);
    }
    printf("\n");
    
    if (*rapdulen < 2 || rapdu[*rapdulen-2] != 0x90 || rapdu[*rapdulen-1] != 0x00)
    {
        printf("CardTransmit - parsing result fault \n");
        goto exitFail;
    }      
    return 0;
    
exitFail:
    return -1;
}

int nfcAccess_SelectNdefTagApplication()
{
    uint8_t capdu[264];
    size_t capdulen;
    uint8_t rapdu[264];
    size_t rapdulen;

    memcpy(capdu, "\x00\xA4\x04\x00\x07\xd2\x76\x00\x00\x85\x01\x01\x00", 13);
    capdulen=13;
    rapdulen=sizeof(rapdu);
    if(nfcAccess_CardTransmit(capdu, capdulen, rapdu, &rapdulen) < 0)
        return -1;
    printf("Application selected!\n");
    return 0;
}

//#error Offset handling
int nfcAccess_ReadBytesFromFile(uint8_t* inBuff, uint16_t offset, uint16_t* byteNo)
{
    uint8_t capdu[264];
    size_t capdulen;
    uint8_t rapdu[264];
    size_t rapdulen;
  
    memcpy(capdu, "\x00\xb0", 2);
    capdu[2] = offset/16;
    capdu[3] = offset%16;
    capdu[4] = *byteNo;
    capdulen=5;
    rapdulen=sizeof(rapdu);
    if(nfcAccess_CardTransmit(capdu, capdulen, rapdu, &rapdulen) < 0)
    {
        printf("Fail to read NDEF file \n");
        return -1;
    }
    printf("File content:\n");
    size_t  szPos;
    for (szPos = 0; szPos < rapdulen-2; szPos++) {
        inBuff[szPos] = rapdu[szPos];
        printf("%02x ", rapdu[szPos]);
    }
    *byteNo = rapdulen-2;
    printf("\n");
    return 0;
}

int nfcAccess_WriteNDEFMsg(uint8_t* ndfMsg, uint16_t offset, uint8_t len)
{
    uint8_t capdu[264];
    size_t capdulen;
    uint8_t rapdu[264];
    size_t rapdulen;
  
    memcpy(capdu, "\x00\xD6", 2);
    capdu[2] = offset/16;
    capdu[3] = offset%16;
    capdu[4] = len;
    memcpy(capdu+5, ndfMsg, len);  
    capdulen=5+len;
    rapdulen=sizeof(rapdu);
    if(nfcAccess_CardTransmit(capdu, capdulen, rapdu, &rapdulen) < 0)
    {
        printf("Fail to write a nfc file \n");
        return -1;
    }
    printf("Writing sucessful!\n");
    return 0;
}

int nfcAccess_SelectFile(uint8_t* id)
{
    uint8_t capdu[264];
    size_t capdulen;
    uint8_t rapdu[264];
    size_t rapdulen;
  
    // Select Capability Container
    memcpy(capdu, "\x00\xa4\x00\x0c\x02", 5);
    memcpy(capdu+5, id, 2);
    capdulen=7;
    rapdulen=sizeof(rapdu);
    if(nfcAccess_CardTransmit(capdu, capdulen, rapdu, &rapdulen) < 0)
    {
        printf("Fail to select a nfc file \n");
        return -1;
    }
    printf("file %02x %02x selected!\n", id[0],id[1]); 
    return 0;
}

void nfcAccess_Close(void)
{
    nfc_close(Pnd);
    nfc_exit(Context);
}