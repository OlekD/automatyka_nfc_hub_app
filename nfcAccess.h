/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   nfc.h
 * Author: aledem
 *
 * Created on 22 marca 2017, 07:37
 */

#ifndef NFC_H
#define NFC_H

#ifdef __cplusplus
extern "C" {
#endif

#define CC_FILE_SIZE                15    
#define CC_FILE_ID                  "\xe1\x03"
    
int nfcAccess_Init(void);
    
void nfcAccess_PullTag(void);

int nfcAccess_CardTransmit(uint8_t * capdu, size_t capdulen, uint8_t * rapdu, size_t * rapdulen);

int nfcAccess_SelectNdefTagApplication();

int nfcAccess_ReadBytesFromFile(uint8_t* inBuff, uint16_t offset, uint16_t* byteNo);

int nfcAccess_WriteNDEFMsg(uint8_t* ndfMsg, uint16_t offset, uint8_t len);

int nfcAccess_SelectFile(uint8_t* id);

void nfcAccess_Close(void);

#ifdef __cplusplus
}
#endif

#endif /* NFC_H */

