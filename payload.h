/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   payload.h
 * Author: aledem
 *
 * Created on 21 marca 2017, 15:04
 */

#ifndef PAYLOAD_H
#define PAYLOAD_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
    
#define PAYLOAD_FILENAME            "/home/kiwi/.config/nfc-payload.conf"
#define PAYLOAD_LINE_MAX_SIZE       100  // Max size of a payload file line
    
int GetPayload(uint8_t* payload, uint8_t* length);


#ifdef __cplusplus
}
#endif

#endif /* PAYLOAD_H */

