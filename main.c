#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <errno.h>
#include <stdbool.h>
#include <unistd.h>

#include "nfcAccess.h"
#include "payload.h"


#define DEBUG_LOG_FILENAME              "/root/debugLog.log"    
//#define DBG_PRINTF(...)     do{\
//                                fprintf(stderr, __VA_ARGS__);	    \
//                                main_DbgFileOpen();		    \
//                                fprintf(DebugLogFile, __VA_ARGS__); \
//                                main_DbgFileClose();		    \
//                            }while(0)           //!< DBG_PRINTF ...

#define DBG_PRINTF(...)     do{\
                                fprintf(stderr, __VA_ARGS__);	    \
                            }while(0)           //!< DBG_PRINTF ...

#define ERR_PRINTF  DBG_PRINTF   

#define NDEF_FILE_BUFF_SIZE         255
#define MAX_PEYLOAD_SIZE            NDEF_FILE_BUFF_SIZE

#define TYPE_LENGTH             (sizeof(TagType))
static const char TagType[] = {'i', 'n', 'n', 'o', 'm', 'e', '.', 'c', 'o', 'm', ':', 'k', 'i', 'w', 'i'};


FILE* DebugLogFile;

void DbgPrintStartupHeader(void);
void main_DbgFileOpen(void);
void main_DbgFileClose(void);

typedef struct ndefFileControlTLV_struct
{
    uint8_t ndefFileID[2];
    uint16_t maxNdefFileSize;
    uint8_t readAccessCondition;
    uint8_t writeAccessCondition;                
}ndefFileControlTLV_t;


int WritePayload(void)
{
    uint8_t payload[MAX_PEYLOAD_SIZE];     
    uint8_t payloadLength;
    uint8_t buff[NDEF_FILE_BUFF_SIZE];     

    if( GetPayload(payload, &payloadLength) < 0 )
    {
        printf("Fail to read a payload \n");
        return -1;
    }

    buff[0] = 0;
    buff[1] = 3+TYPE_LENGTH+payloadLength;  // Total length of NDEF message (all the next bytes)
    // start of the NDEF message and the first NDEF record.
    buff[2] = 0xD4;     // 0xD4: MB=1, ME=1 CF=0, SR=1, IL=0, TNF=4 NFC Forum external type
    buff[3] = TYPE_LENGTH;    
    buff[4] = payloadLength;
    memcpy(buff+5, TagType, TYPE_LENGTH);   
    memcpy(buff+5+TYPE_LENGTH, payload, payloadLength);
    // end of the first NDEF record
    
    printf("TYPE_LENGTH :%d, payloadLength: %d\n", TYPE_LENGTH, payloadLength);
    if(nfcAccess_WriteNDEFMsg(buff, 0, 5+TYPE_LENGTH+payloadLength) < 0)
    {
        printf("Fail to write a payload \n");
        return -1;
    }
    return 0;
}

uint16_t Bigendian2uint16(uint8_t* table)
{
    return (table[0]*16 + table[1]);
}

uint16_t ReadNfcFileLength(void)
{
    uint8_t FileLENBuff[2];
    uint16_t FileLength;
    uint16_t inByteNo = 2;
    
    if(nfcAccess_ReadBytesFromFile(FileLENBuff, 0, &inByteNo) < 0)
    {
        printf("Fail to read nfc file length \n");
        return -1;
    }
    FileLength = Bigendian2uint16(FileLENBuff);
    return(FileLength);
}

bool CheckNDEFLength(uint16_t length, uint16_t maxLength)
{
    /* If NLEN>0000h and NLEN≤Maximum NDEF file size-2, the NDEF message is detected
     *    inside the Type 4 Tag Platform */
    
    if((length > 0) && (length < (maxLength-2) ))
        return true;
    else
        return false;
}



int ReadNdefControlTLV(ndefFileControlTLV_t *ndefFileControlTLV)
{   
    uint8_t capabilityContainer[CC_FILE_SIZE];
    uint16_t inByteNo;
    uint8_t NDEFFileControlTLVBuff[8];
    
    if(nfcAccess_SelectFile(CC_FILE_ID) < 0)
    {
        goto exitFail; 
    }        
    inByteNo = ReadNfcFileLength();
    if(inByteNo < 0)
    {
        goto exitFail;        
    }
    if(inByteNo > CC_FILE_SIZE)
    {
        printf("WARNING Not all data read from the Capability Container /n");
        inByteNo = CC_FILE_SIZE;
    }
    if(nfcAccess_ReadBytesFromFile(capabilityContainer, 0, &inByteNo) < 0)
    {
        goto exitFail;       
    }
    memcpy(NDEFFileControlTLVBuff, capabilityContainer+7, 8);
    
    memcpy(ndefFileControlTLV->ndefFileID, NDEFFileControlTLVBuff+2, 2);
    ndefFileControlTLV->maxNdefFileSize = Bigendian2uint16(NDEFFileControlTLVBuff+4);
    ndefFileControlTLV->readAccessCondition = NDEFFileControlTLVBuff[6];
    ndefFileControlTLV->writeAccessCondition = NDEFFileControlTLVBuff[7]; 
    return 0;
    
exitFail:
    printf("Fail to Read Ndef Control TLV \n");
    return -1;
}

bool CheckNdefControlTLV(ndefFileControlTLV_t *ndefFileControlTLV)
{
    bool result = true;
    if(ndefFileControlTLV->readAccessCondition != 0) // indicates read access granted without any security 
        result = false;
    if(ndefFileControlTLV->writeAccessCondition != 0) // indicates write access granted without any security 
        result = false;
    return(result);
}

int ReadNdefFile(uint8_t *ndefFile, ndefFileControlTLV_t *ndefFileControlTLV)
{
    uint16_t inByteNo;
    
    // Select NDEF file
    if(nfcAccess_SelectFile(ndefFileControlTLV->ndefFileID) < 0)
    {
        goto exifFail;
    }     
    inByteNo = ReadNfcFileLength();
    // printf("NDEFLEN: %d \n", inByteNo);
    if(CheckNDEFLength(inByteNo, ndefFileControlTLV->maxNdefFileSize) == false)
    {
        printf("Wrong NDEF length \n");
        goto exifFail;
    }
    if(inByteNo > NDEF_FILE_BUFF_SIZE)
    {
        printf("WARNING Not all data read from the NDEF File");
        inByteNo = NDEF_FILE_BUFF_SIZE;
    }
    if(nfcAccess_ReadBytesFromFile(ndefFile, 2, &inByteNo) < 0)
    {
        goto exifFail;
    }
    return 0;
exifFail:
    printf("Fail to read NDEF file \n");
    return -1;
}

int main_nfc(void)
{
    ndefFileControlTLV_t ndefFileControlTLV;
    uint8_t ndefFile[NDEF_FILE_BUFF_SIZE];
    
    nfcAccess_Init();
    
    while(1)
    {
        nfcAccess_PullTag();   
        if(nfcAccess_SelectNdefTagApplication() < 0)
        {
            printf("ERROR  Fail to select NDEF tag application \n");
            continue;
        }
        if(ReadNdefControlTLV(&ndefFileControlTLV) < 0)
        {
            printf("ERROR  Fail to Read Ndef Control TLV \n");
            continue;            
        }
        if(CheckNdefControlTLV(&ndefFileControlTLV) == false)
        {
            printf("ERROR  Wrong Value in NDEF Control TLV \n");
            continue;
        }            
        ReadNdefFile(ndefFile, &ndefFileControlTLV);    
        if(WritePayload() < 0)
        {
            printf("ERROR  Fail to write a key \n");
            continue;            
        }
        fflush(stdout);
        sleep(5);
    }
    nfcAccess_Close();
    exit(EXIT_SUCCESS);
}


int main (int argc, char *argv[])
{
    DbgPrintStartupHeader();
    main_nfc();
}



void main_DbgFileOpen(void)
{
    DebugLogFile = fopen(DEBUG_LOG_FILENAME, "a+");  //Open for reading and appending (writing at end of file).
    if(!DebugLogFile)
    {
        fprintf(stderr, "main_DbgFileOpen() - file does not exist \n");
        while(1) ;
    }
}

void main_DbgFileClose(void)
{
    fclose(DebugLogFile);
}

void DbgPrintStartupHeader(void)
{
    time_t t = time(NULL);
    char* dateTime = (char*)ctime(&t);

    *(dateTime+strlen(dateTime)-1) = '\0';  // remove the new line character from the end of dateTime string.
    DBG_PRINTF("============================== LIBNFC %s ==========================================\n", dateTime);
}

