/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   payload.c
 * Author: aledem
 *
 * Created on 21 marca 2017, 14:01
 */

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include "payload.h"

#define CRC_SIZE (sizeof(crc))

static int GetPeyloadLength(FILE * pFile);
static bool IsEndOfLine(char c);
//bool IsHexRange(char c);
static int cxtoi(char c, char *i);
static int ParseLine(char *buff, int buffSize, uint8_t *payload, uint8_t length, uint8_t *writeIndex);
static unsigned short crc16(char *data_p, unsigned short length);


int GetPayload(uint8_t* payload, uint8_t* length)
{
    FILE * pFile;
    uint8_t len;
    uint16_t crc;
    uint8_t payloadWriteIndex = 0;
    char lineBuff[PAYLOAD_LINE_MAX_SIZE];
    int lineBuffSize = PAYLOAD_LINE_MAX_SIZE;

    pFile = fopen(PAYLOAD_FILENAME, "r");

    if (pFile==NULL)
    {
        perror ("Error opening file");
        return 1;
    }    
    len = GetPeyloadLength(pFile);
    if(len < 0)
    {
        perror ("Error reading payload - wrong length");
        return 1;
    }
    printf("Payload len: %d \n", len);
    
    do {        
        fgets(lineBuff, lineBuffSize, pFile);
        if(lineBuff == NULL)
        {
            break;
        }
        if(ParseLine(lineBuff, lineBuffSize, payload, len, &payloadWriteIndex))
        {
            return 1;
        }
        if(len == payloadWriteIndex)
        {
            break;
        }
    } while (1);
    fclose (pFile);
    crc = crc16(payload, len);
    payload[len] = (uint8_t)((crc >> 8) & 0xFF);   // memcpy can not be used due to wrong endian.
    payload[len+1] = (uint8_t)(crc & 0xFF);
    *length = len + CRC_SIZE;      // plus crc length
    
    return 0;
}

static int GetPeyloadLength(FILE * pFile)
{
    char buff[PAYLOAD_LINE_MAX_SIZE];
    char c;
    int i, buffsize = PAYLOAD_LINE_MAX_SIZE;
    uint8_t value = 0;
    
    fgets(buff, buffsize, pFile);
    if(buff == NULL)
    {
        perror ("Error reading file - empty file");
        return -1;
    }
    for(i=0; i<buffsize; i++)
    {
        c = buff[i];
        if((c == ' ') || (c == '#') || (c == '\0')
                || (c == '\n') || (c == '\t'))
        {
            break;
        }
        if((c > '9') || (c < '0'))
        {
            perror ("Error reading file - wrong payload length");
            return -1;
        }
        value = (value * 10) + (c - '0');
    }
    return value;
}

static bool IsEndOfLine(char c)
{
    if((c == ' ') || (c == '#') || (c == '\0')
        || (c == '\n') || (c == '\t'))
    {
        return true;
    }
    return false;
}

//bool IsHexRange(char c)
//{
//    if( ((c >= 'a') && (c <= 'f')) 
//            || ((c >= 'A') && (c <= 'F')) 
//            || ((c >= '0') && (c <= '9')) )
//    {
//        return true;
//    }
//    return false;            
//}

static int cxtoi(char c, char *i)
{
    if((c >= '0') && (c <= '9'))
    {
        *i = c - '0';
    }
    else if((c >= 'A') && (c <= 'F'))
    {
        *i = c - 'A' + 10;
    }
    else if((c >= 'a') && (c <= 'f'))
    {
        *i = c - 'a' + 10;
    }
    else
    {
        return 1;
    }
    return 0;
}

static int ParseLine(char *buff, int buffSize, uint8_t *payload, uint8_t length, uint8_t *writeIndex)
{
    int charCnt;
    char c1, c2;
    char i1, i2;
        
    for(charCnt = 0; charCnt < buffSize; )
    {
        c1 = buff[charCnt];
        c2 = buff[charCnt+1];
        if(IsEndOfLine(c1))
        {
            break;
        }
        if(IsEndOfLine(c2))
        {
            perror ("Error reading file - End of line in wrong place");
            return 1;
        }
        if((c1 == '0') && (c2 == 'x'))
        {
            charCnt += 2;
            continue;
        }
        if( cxtoi(c1, &i1) || cxtoi(c2, &i2) )
        {
            perror ("Error reading file - wrong file format - not hex val");
            return 1;
        }
        if(*writeIndex == length)
        {
           perror ("Error reading file - wrong number of data");
           return 1;
        }
        payload[*writeIndex] = (i1 * 16) + i2;
        (*writeIndex)++;
        charCnt += 2;        
    }    
    return 0;
}

#define POLY 0x8408
/*
//                                      16   12   5
// this is the CCITT CRC 16 polynomial X  + X  + X  + 1.
// This works out to be 0x1021, but the way the algorithm works
// lets us use 0x8408 (the reverse of the bit pattern).  The high
// bit is always assumed to be set, thus we only use 16 bits to
// represent the 17 bit value.
*/

unsigned short crc16(char *data_p, unsigned short length)
{
      unsigned char i;
      unsigned int data;
      unsigned int crc = 0xffff;

      if (length == 0)
            return (~crc);

      do
      {
            for (i=0, data=(unsigned int)0xff & *data_p++;
                 i < 8; 
                 i++, data >>= 1)
            {
                  if ((crc & 0x0001) ^ (data & 0x0001))
                        crc = (crc >> 1) ^ POLY;
                  else  crc >>= 1;
            }
      } while (--length);

      crc = ~crc;
      data = crc;
      crc = (crc << 8) | (data >> 8 & 0xff);

      return (crc);
}